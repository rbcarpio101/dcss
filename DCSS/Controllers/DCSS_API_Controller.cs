﻿using DCSS.Models;
using MySql.Data;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DCSS.Controllers
{
    public class DCSS_API_Controller : Controller
    {
       public static string connection = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;

        MySqlConnection con = new MySqlConnection(connection);

        //
        // GET: /DCSS_API_/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult getQuestions(int respid)
        {
            List<Questions> data = new List<Questions>();
            
            MySqlCommand cmd = new MySqlCommand("sp_GetQuestions",con);
            cmd.CommandTimeout = 600;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("respID",respid);
            con.Open();

            MySqlDataReader r = cmd.ExecuteReader();


            while(r.Read()){

                Questions q = new Questions();
                q.id = (int)r["id"];
                q.question = r["question"].ToString();
                q.respid = Convert.ToInt32(r["respondentID"].ToString());

                data.Add(q);
            }

            con.Close();

            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getScale()
        {

            List<ScaleOfImportance> data = new List<ScaleOfImportance>();

            MySqlCommand cmd = new MySqlCommand("sp_GetScale", con);
            cmd.CommandTimeout = 600;

            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("respID", respid);
            con.Open();

            MySqlDataReader r = cmd.ExecuteReader();


            while (r.Read())
            {

                ScaleOfImportance q = new ScaleOfImportance();
                q.id = (int)r["ID"];
                q.orderID = (int)r["orderID"];
                q.name = r["name"].ToString();

                data.Add(q);
            }

            con.Close();

            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }


        public string saveAnswer(string answers, string passcode, int respid, int personid)
        {
             string res = "";
             try
             {
                 var i = answers.Split('|').Count();

                 for (var x = 0; x < (i - 1); x++)
                 {
                     Answer a = new Answer();

                     a.questionID = Convert.ToInt32(answers.Split('|')[x].Split(':')[0]);
                     a.scaleID = Convert.ToInt32(answers.Split('|')[x].Split(':')[1]);
                     a.respID = respid;
                     a.ddate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                     a.personID = personid;
                     a.passcode = passcode;

                     InsertAnswerToDB(a);

                 }
                 res = "1";
             }
             catch (Exception ex)
             {
                 res = ""+ex;
             }
             

             return res;
        }


        public string InsertAnswerToDB(Answer a)
        {
            var res = "";
            try
            {
                MySqlCommand cmd = new MySqlCommand("sp_SaveAnswer", con);
                cmd.CommandTimeout = 600;

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("qID", a.questionID);
                cmd.Parameters.AddWithValue("sID", a.scaleID);
                cmd.Parameters.AddWithValue("rID", a.respID);
                cmd.Parameters.AddWithValue("ddate", a.ddate);
                cmd.Parameters.AddWithValue("pID", a.personID);
                cmd.Parameters.AddWithValue("passcode", a.passcode);

                con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

                res = "1";
            }
            catch (Exception ex)
            {
                res = "" + ex;
            }

            return res;
        }

    }
}

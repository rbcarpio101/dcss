﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DCSS.Models
{
    public class Answer
    {
        public int id { get; set; }
        public int questionID { get; set; }
        public int scaleID { get; set; }
        public int respID { get; set; }
        public DateTime ddate { get; set; }
        public int personID { get; set; }
        public string passcode { get; set; }
    }
}
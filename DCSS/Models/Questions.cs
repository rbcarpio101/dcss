﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DCSS.Models
{
    public class Questions
    {
        public int id { get; set; }
        public string question { get; set; }
        public int respid { get; set; }
    }
}
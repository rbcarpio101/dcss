﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DCSS.Models
{
    public class ScaleOfImportance
    {
        public int id { get; set; }
        public int orderID { get; set; }
        public string name { get; set; }
    }
}